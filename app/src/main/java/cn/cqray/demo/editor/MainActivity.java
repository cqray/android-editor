package cn.cqray.demo.editor;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import cn.cqray.android.editor.KeyboardUtils;
import cn.cqray.android.editor.RichEditor;
import cn.cqray.android.editor.EditorMenuBar;


public class MainActivity extends AppCompatActivity {

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RichEditor editor = findViewById(R.id.wv_start);
        KeyboardUtils.showSoftInput(editor);
        EditorMenuBar richEditorActionBar = findViewById(R.id.action);
        richEditorActionBar.setRichEditor(editor);
        editor.insertText("hello world!");
    }

}
